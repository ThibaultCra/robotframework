*** Settings ***
Library    Selenium2Library    
Resource    ../Conf/conf_infos_galaxy_s5.robot

*** Keywords ***

!AccesAssistance
    Open Browser    ${URL_ASSISTANCE}    chrome  
    Maximize Browser Window  
    Set Browser Implicit Wait    5
  
!ChoisirTypeDemande
    Wait Until Element Is Visible    ${LIEN_DEPANNAGE}    20
    Click Link    ${LIEN_DEPANNAGE}
    
!ChoixTypeAppareil
    Sleep    2
    Wait Until Page Contains Element    //*[@id="nav_mobile-tablette"]/a/span    20
    Click Element    //*[@id="nav_mobile-tablette"]/a/span    
    
!RemplirForm
    Wait Until Element Is Visible    xPath=//*[@id="n0"]/div/div[1]/div/div/span/a    20
    Click Link    xPath=//*[@id="n0"]/div/div[1]/div/div/span/a
    Wait Until Element Is Visible    xPath=//*[@id="n1"]/div/div[10]/div/div/span/a    20
    Click Link    xPath=//*[@id="n1"]/div/div[10]/div/div/span/a  
    Wait Until Element Is Visible    id=brand    20
    Select From List By Label    id=brand    Samsung   
    Wait Until Element Is Visible    id=device     20
    Select From List By Label    id=device    Galaxy S5 (SM-G900F) 
    Wait Until Element Is Enabled    //*[@id="n2"]/div/ul/li[7]/a   20 
    Sleep    5
    Click Element    xpath=//*[@id="n2"]/div/ul/li[7]/a    
    
!QuitterPage
    Close Browser
# !RenseignerInfos
    
# !Deconnection
    
# !RenseignerMotifs
    
# !RenseignerInfosAppareil
    
# !RenseignerActionSouhaitee