*** Settings ***
Resource    ../PageObject/pageobject_infos_galaxy_s5.robot

*** Keywords ***

AccesAssistance
    !AccesAssistance
    
AccesDepannage
    !ChoisirTypeDemande
    !ChoixTypeAppareil
    
RemplirFormulaire
    !RemplirForm
    
QuitterPage
    !QuitterPage