*** Settings ***
Library    Selenium2Library   
Suite Setup    Log    i am inside test suite setup
Suite Teardown    Log    i am inside test suite teardown
Test Setup    Log    i am inside test setup
Test Teardown    Log    i am inside test teardown
Default Tags    sanity

*** Test Cases ***
MyFirstTest
    [Tags]    smoke
    Log    First ! 
    
MySecondTest
    [Tags]    example
    Log    Second !
    Set Tags    regression1
    Remove Tags    regression1
    
MyThirdTest
    Log    Third !
    
FirstSeleniumTest
    Open Browser    https://google.com    chrome
    Set Browser Implicit Wait    5
    Input Text    name=q    wololo
    Press Keys    name=q    ENTER
    #Click Button    name=btnK    
    Sleep    4   
    Close Browser
    Log    Test completed
    
SampleLoginTest
    [Documentation]    This is a sample login test
    Open Browser    ${URL}    chrome    
    Set Browser Implicit Wait    5
    LoginKW
    Click Element    id=welcome
    Click Element    link=Logout   
    Close Browser  
    Log    Test completed 
    Log    ${TEST NAME} was executed by %{username} on %{os}
    
*** Variables ***
${URL}    https://opensource-demo.orangehrmlive.com/
@{CREDENTIALS}    Admin    admin123
&{LOGINDATA}    username=Admin    password=admin123

*** Keywords ***
LoginKW
    Input Text    id=txtUsername    @{CREDENTIALS}[0]
    Input Text    id=txtPassword    &{LOGINDATA}[password]
    Click Button    id=btnLogin
